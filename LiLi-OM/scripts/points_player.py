#!/usr/bin/python3

from time import sleep
import numpy as np
import rospy
import sensor_msgs.point_cloud2 as pc2
import std_msgs


#used PointXYZINormal structure:[x,y,z,normal_x, normal_y, normal_z, intensity, curvature,time]
#information stored:            [x,y,z,id,line,dynamic,rel_time, curvature, frame_time]

input_path:str=""
output_topic:str="/points"

point_fields=[
    pc2.PointField('x',0,pc2.PointField.FLOAT32,1),
    pc2.PointField('y',4,pc2.PointField.FLOAT32,1),
    pc2.PointField('z',8,pc2.PointField.FLOAT32,1),
    pc2.PointField('normal_x',12,pc2.PointField.FLOAT32,1),
    pc2.PointField('normal_y',16,pc2.PointField.FLOAT32,1),
    pc2.PointField('normal_z',20,pc2.PointField.FLOAT32,1),
    pc2.PointField('intensity',24,pc2.PointField.FLOAT32,1),
    pc2.PointField('curvature',28,pc2.PointField.FLOAT32,1),
]

def load_points(pcl_path:str):
    "loads and returns numpy array storing all point clouds"
    data_points=None
    with open(pcl_path,"rb") as input_file:
        data_points=np.load(input_file)
    print("Point cloud loaded successfully")
    return data_points

def get_times(points):
    "returns start and end times of given pcl sequence"
    time_column=points[:,8]
    min_time=min(time_column)
    max_time=max(time_column)
    return min_time,max_time

def get_timeframe(points,time):
    "returns matrix containing points captured at given time"
    time_indices= abs(points[:,8] - time)<0.09
    timeframe=points[time_indices,0:8]
    return timeframe

def numpy_to_msg(frame,time:float)->pc2.PointCloud2:
    "returns given point cloud converted to ros message"
    msg_header=std_msgs.msg.Header()
    msg_header.stamp=rospy.Time.from_sec(time)
    msg_header.frame_id="lili_om"
    msg_out=pc2.create_cloud(msg_header,point_fields,frame)
    return msg_out

def main():
    rospy.init_node("PointsPlayer")
    pub_pcl=rospy.Publisher(output_topic,pc2.PointCloud2,queue_size=200)
    #load launch file params:
    input_path=rospy.get_param("files/input_points",default=" ")
    rate=rospy.get_param("files/playback_rate",default=1)
    
    point_cloud=load_points(input_path)
    publish_rate = rospy.Rate(rate)
    min_time,max_time=get_times(point_cloud)
    time = min_time
    print("PointsPlayer Started - "+str(rate)+" fps")
    sleep(2)
    while not rospy.is_shutdown() and time<= max_time+0.1:
        timeframe=get_timeframe(point_cloud,time)
        msg=numpy_to_msg(timeframe,time)
        pub_pcl.publish(msg)
        print("published: "+str(time))
        publish_rate.sleep()
        time += 0.1
    print("Points playback finished")
    if not rospy.is_shutdown():
        print("Press Ctrl + C to exit and close Rviz")
    rospy.spin()

if __name__ == "__main__":
    main()