#!/usr/bin/python3

import rospy
import numpy as np
from geometry_msgs.msg import Point, Pose,Quaternion
from nav_msgs.msg import Odometry
from sensor_msgs import point_cloud2
import xml.etree.ElementTree as et
import tf.transformations as tr



class PosesLauncher:
    def __init__(self):
        pub_topic_name = "/poses"
        sub_topic_name = "/lidar_cloud_dynamic"
        self.poses_pub=rospy.Publisher(pub_topic_name,Odometry,queue_size=900)
        self.preprocessing_sub=rospy.Subscriber(sub_topic_name,point_cloud2.PointCloud2,self.pcl_callback)
        self.if_to_debug=rospy.get_param("common/if_to_debug",default=False)
        self.file_path=rospy.get_param("files/input_poses",default=" ")
        self.pose_idx=0
        if(self.if_to_debug):
            print("Loading poses")
        pcl_poses,frame_len=self.load_poses()
        if(self.if_to_debug):
            print(frame_len)
            print("Poses loaded successfully  ---- READY TO START ----")
        self.poses_cnt=frame_len
        self.poses=pcl_poses

    def load_poses(self):
        "retrieves poses included in xml file"
        result=[]
        xml_file=et.parse(self.file_path)
        root=xml_file.getroot()
        odom=root.find('odometry')
        raw_poses=odom.findall('matrix')
        for raw_pose in raw_poses:
            time=float(raw_pose.attrib['time'])
            coords=list(map(float,raw_pose.text[1:-1].split(' ')))
            abs_pose=np.reshape(coords,[4,4])
            result.append((time,abs_pose))  

        return result,len(result)
    
    
    def pcl_callback(self,point_cloud):
        "publish new pose corresponding to each incoming point cloud"
        if self.pose_idx<self.poses_cnt:
            #retrieve current pose
            current_time,pose_matrix=self.poses[self.pose_idx]
            rotation_matrix=pose_matrix[0:3,0:3]
            translation_vector=pose_matrix[0:3,3]
            self.pose_idx+=1
            rot_mat=np.identity(4)
            rot_mat[0:3,0:3]=rotation_matrix
            temp_quat=tr.quaternion_from_matrix(rot_mat)
            #construct new message
            pose=Odometry()
            pose.header.stamp=point_cloud.header.stamp
            pose.header.frame_id="lili-om"
            pose_point=Point(translation_vector[0],translation_vector[1],translation_vector[2])
            pose_quat=Quaternion(temp_quat[0],temp_quat[1],temp_quat[2],temp_quat[3])
            pose.pose.pose=Pose(pose_point,pose_quat)
            pose.header.stamp=rospy.Time.from_seconds(current_time/1000000)
            #publish message
            self.poses_pub.publish(pose)
            if(self.if_to_debug):
                print("published pose:   "+ str(translation_vector))

def main():
    rospy.init_node("poses_publisher")
    poses_launcher=PosesLauncher()
    rospy.spin()
    
    
if __name__ == "__main__":
    main()
