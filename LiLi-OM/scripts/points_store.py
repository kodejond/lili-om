#!/usr/bin/python3

import rospy
import numpy as np
from sensor_msgs import point_cloud2 as pc2
import os


#used PointXYZINormal structure:[x,y,z,normal_x, normal_y, normal_z, intensity, curvature,time]
#information stored:            [x,y,z,id,line,dynamic,rel_time, curvature, frame_time]

source_topic:str = "/dynamic_surf"

res:np.array
msg_buffer=[]
output_path:str=rospy.get_param("/files/output_points","")
if_to_debug:bool=rospy.get_param("/common/if_to_debug",default=False)
kill_on_exit:bool=rospy.get_param("/common/kill_on_exit",default=False)
saved_pts_cnt:int=0

def correct_ground(raw_frame):
    "Labels all ground point (-1) back to static (0)"
    gnd_row_indices=raw_frame[:,5]==-1
    raw_frame[gnd_row_indices,5]=0
    if(if_to_debug):
        print("found: " +str(np.sum(gnd_row_indices))+" ground points")

def process_points(point_cloud,frame_time):
    "formats PointCloud2 into numpy matrix and appends to result"
    global res
    global saved_pts_cnt
    pt_gen=pc2.read_points(point_cloud,skip_nans=True)
    pt_list=np.array(list(pt_gen))
    if pt_list.size == 0:
        if(if_to_debug):
            print("Skipped: " + str(frame_time))
        return
    correct_ground(pt_list)
    time_column=frame_time*np.ones([pt_list.shape[0],1],dtype=np.float32)
    pt_list=np.append(pt_list,time_column,axis=1)
    new_pts_cnt=pt_list.shape[0]
    res[saved_pts_cnt:(saved_pts_cnt+new_pts_cnt),:]=pt_list
    saved_pts_cnt+=new_pts_cnt
    #res=np.concatenate((res,pt_list),axis=0,dtype=np.float32)
    if(if_to_debug):
        print("processed : "+str(frame_time) +" / "+str(res.shape))

def save_points():
    "saves buffered point clouds to numpy point cloud"
    global res
    global saved_pts_cnt
    min_time=msg_buffer[0][0]
    max_time=msg_buffer[-1][0]
    time_delta=max_time-min_time
    pts_count=(round(time_delta/0.1)+1)*96000
    res=np.empty([pts_count,9],dtype=np.float32)
    for buffer_item in msg_buffer:
        #try to detect empty message here
        process_points(buffer_item[1],buffer_item[0])

    final_result=res[0:saved_pts_cnt,:]
    with open(output_path,"wb") as out_file:
        np.save(out_file,final_result)
    print("Points saved to: "+output_path)
    print("Press Ctrl+C to exit")
    if(kill_on_exit):
        os.system("pkill ros")

def register_points(msg_in):
    "adds incoming timestamped point cloud to msg buffer"
    global msg_buffer
    frame_time=msg_in.header.stamp.to_sec()
    if(if_to_debug):
        print("recieved : "+str(frame_time))
    buffer_item=[frame_time,msg_in]
    msg_buffer.append(buffer_item)
    if(msg_in.header.frame_id=="liliend"):
        #reached last point cloud msg --> save points
        save_points()

def main():
    rospy.init_node("PointsStore")
    sub_src = rospy.Subscriber(source_topic,pc2.PointCloud2,register_points)
    print("Points Store STARTED")
    rospy.spin()
    
    

if __name__ == "__main__":
    main()



