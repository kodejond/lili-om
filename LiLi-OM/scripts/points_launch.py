#!/usr/bin/python3

from time import sleep
import numpy as np
import sensor_msgs.point_cloud2 as pc2
import std_msgs.msg
import rospy

input_file:str=""
pcl_topic:str="/points"


#used PointXYZINormal structure:[x,y,z,normal_x, normal_y, normal_z, intensity, curvature,time]
#information stored:            [x,y,z,id,line,dynamic,rel_time, curvature, frame_time]

point_fields=[
    pc2.PointField('x',0,pc2.PointField.FLOAT32,1),
    pc2.PointField('y',4,pc2.PointField.FLOAT32,1),
    pc2.PointField('z',8,pc2.PointField.FLOAT32,1),
    pc2.PointField('normal_x',12,pc2.PointField.FLOAT32,1),
    pc2.PointField('normal_y',16,pc2.PointField.FLOAT32,1),
    pc2.PointField('normal_z',20,pc2.PointField.FLOAT32,1),
    pc2.PointField('intensity',24,pc2.PointField.FLOAT32,1),
    pc2.PointField('curvature',28,pc2.PointField.FLOAT32,1),
]

def load_points(pcl_path:str):
    "loads and returns numpy array storing all point clouds"
    data_points=None
    with open(pcl_path,"rb") as input_file:
        data_points=np.load(input_file)
    max_time:np.float32=np.max(data_points[:,8],0)
    print("Data loaded successfully -- max time: " + str(max_time) )
    return data_points,max_time

def extract_timeframe(points:np.array,timestamp:float):
    "selects all dynamic points of a given timestamp"
    time_indices = np.abs(points[:,8]-timestamp)<0.09
    timeframe=points[time_indices,:]
    return timeframe

def extract_lidar_frame(timeframe, lidar_id)->np.array:
    "returns points from given lidar extracted from given timeframe"
    lidar_indices=timeframe[:,3]==lidar_id
    lidar_frame=timeframe[lidar_indices]
    return lidar_frame


def extract_frames(points:np.array,timestamp:float):
    "returns ordered list of dynamic points at corresponding lidar index"
    time_frame=extract_timeframe(points,timestamp)
    result=[]
    for lidar_id in range(1,4):
        lidar_frame=extract_lidar_frame(time_frame,lidar_id)
        result.append(lidar_frame)
    return result


def numpy_to_msg(frame,time:float)->pc2.PointCloud2:
    "returns given point cloud converted to ros message"
    #msg_out=pypcd.numpy_pc2.array_to_pointcloud2(frame,rospy.Time(time),"lili_om")
    msg_header=std_msgs.msg.Header()
    msg_header.stamp=rospy.Time.from_sec(time)
    msg_header.frame_id="lili_om"
    msg_out=pc2.create_cloud(msg_header,point_fields,frame)
    return msg_out

def get_times(points):
    "returns start and end times of given pcl sequence"
    time_column=points[:,8]
    min_time=min(time_column)
    max_time=max(time_column)
    return min_time,max_time

def main():
    rospy.init_node("PointsLauncher")
    #load launch file params:
    input_file=rospy.get_param("/files/input_points",default=" ")
    launch_rate_cnt=rospy.get_param("/files/playback_rate",default=10)

    launch_rate=rospy.Rate(launch_rate_cnt)
    points,end_time=load_points(input_file)
    start_time,end_time=get_times(points)
    pub_cloud=rospy.Publisher(pcl_topic,pc2.PointCloud2,queue_size=800)
    time:float=start_time
    sleep(10)
    while not rospy.is_shutdown() and time<=end_time + 0.1:
        lidar_frames=extract_frames(points,time)
        for lidar_id in range(3):
            cur_frame=lidar_frames[lidar_id]
            #publish lidar points:
            if cur_frame.size==0:
                print("Skipping empty frame")
            else:
                msg_dynamic=numpy_to_msg(cur_frame[:,0:8],time)
                pub_cloud.publish(msg_dynamic)
        time += 0.1
        launch_rate.sleep()
    print("Points Launch finished - no points left - press Ctrl+C to save and exit")
    rospy.spin()



if __name__ == "__main__":
    main()
