#!/usr/bin/python3

import rospy
import sensor_msgs.point_cloud2 as pc2
import numpy as np

path:str=" "
dyn_top:str="/corrected_dynamic_points"
stat_top:str="/lidar_cloud_static"


class Collector:

    def register_static(self,msg_in:pc2.PointCloud2):
        time=msg_in.header.stamp.to_sec()
        pt_gen=pc2.read_points(msg_in,skip_nans=True)
        pt_arr=np.array(list(pt_gen),dtype=np.float32)
        time_column=np.ones([pt_arr.shape[0],1],dtype=np.float32) * time
        pt_arr=np.append(pt_arr,time_column,axis=1)
        if(self.static_array.size==0):
            self.static_array=pt_arr
        else:
            self.static_array=np.concatenate((self.static_array,pt_arr),axis=0,dtype=np.float32)
        if(self.if_to_debug):
            print("Registered static points: " + str(time))

    def register_dynamic(self,msg_in:pc2.PointCloud2):
        time=msg_in.header.stamp.to_sec()
        pt_gen=pc2.read_points(msg_in,skip_nans=True)
        pt_arr=np.array(list(pt_gen),dtype=np.float32)
        time_column=np.ones([pt_arr.shape[0],1],dtype=np.float32) * time
        pt_arr=np.append(pt_arr,time_column,axis=1)
        if(self.dynamic_array.size==0):
            self.dynamic_array=pt_arr
        else:
            self.dynamic_array=np.concatenate((self.dynamic_array,pt_arr),axis=0,dtype=np.float32)
        if(self.if_to_debug):
            print("Registered dynamic points: " + str(time))
        

    def save_points(self):
        final_array=np.concatenate((self.dynamic_array,self.static_array),dtype=np.float32,axis=0)
        if self.dynamic_array.size>0 or self.static_array.size>0:
            with open(self.output_path,'wb') as out_file:
                np.save(out_file,final_array)
        print("Saved corrected points to: " + self.output_path)

    def __init__(self,static_pts_topic:str,dynamic_pts_topic:str):
        self.dynamic_array=np.array([])
        self.static_array=np.array([])
        self.if_to_debug=rospy.get_param("/common/if_to_debug",default=False)
        self.output_path=rospy.get_param("/files/output_points",default="")
        self.static_sub=rospy.Subscriber(static_pts_topic,pc2.PointCloud2,self.register_static,queue_size=200)
        self.dynamic_sub=rospy.Subscriber(dynamic_pts_topic,pc2.PointCloud2,self.register_dynamic,queue_size=200)
        rospy.on_shutdown(self.save_points)



def main():
    rospy.init_node("PointsCollection")
    collector=Collector(stat_top,dyn_top)
    print("Points Collection STARTED")
    rospy.spin()


if __name__ == "__main__":
    main()