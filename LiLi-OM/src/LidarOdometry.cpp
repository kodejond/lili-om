#include "utils/common.h"
#include "utils/math_tools.h"
#include "utils/timer.h"
#include "factors/LidarKeyframeFactor.h"


class LidarOdometry {
private:
    int odom_pub_cnt = 0;

    ros::NodeHandle nh;

    //subscriptions to Preprocessing
    ros::Subscriber sub_surf;
    ros::Subscriber sub_full_cloud;

    //final publisher
    ros::Publisher pub_full_cloud;

    //debugging publishers
    ros::Publisher pub_lc_map;
    ros::Publisher pub_matched_pts;
    ros::Publisher pub_trashed_pts;
    ros::Publisher pub_surf;

    std_msgs::Header cloud_header; //last recieved cutted point cloud's header

    pcl::PointCloud<PointType>::Ptr surf_features; //surf points from PREPROCESSING
    pcl::PointCloud<PointType>::Ptr full_cloud; //cutted point cloud from PREPROCESSING
    PointPoseInfo *cur_real_pose;

    pcl::PointCloud<PointType>::Ptr surf_last_ds; //downsampled incoming surface feature points pont cloud

    pcl::KdTreeFLANN<PointType >::Ptr kd_tree_surf_last; //data structure for nearest neighbour search in local map

    double rel_pose[7];

    bool system_initialized;

    int surf_res_cnt; //number of surface feature points -- that are currently being matched

    int max_num_iter; //limit for solver iterations -- set by parameters
    int scan_match_cnt; //iterations of scan-matching -- set by parameters
    double solver_time_limit; //max time for Ceres solver -- set by parameters

    vector<pcl::PointCloud<PointType>::Ptr> surf_frames;

    deque<pcl::PointCloud<PointType>::Ptr> recent_surf_frames;

    pcl::PointCloud<PointType>::Ptr surf_from_map; //pointcloud containing transformed surface points from last 20 frames 
    pcl::PointCloud<PointType>::Ptr surf_from_map_ds; //downsampled surfaces map

    pcl::PointCloud<PointType>::Ptr surf_current_pts; //currently matched surface feature points
    pcl::PointCloud<PointType>::Ptr surf_normal; //matched planes represented by points

    std::deque<pcl::PointCloud<PointType>::Ptr> new_full_clouds; //incoming full clouds buffer
    std::deque<pcl::PointCloud<PointType>::Ptr> new_surfs; //incoming surface features clouds buffer

    int latest_frame_idx=0;
    size_t processed_frames=0;

    string frame_id = "lili_om";
    double runtime = 0;

    double weight_treshold;
    double plane_treshold;
    double nn_dist_tresh;
    int map_frames_cnt;
    bool if_to_debug;

public:
    LidarOdometry(): nh("~") {
        initializeParameters();
        allocateMemory();

        //SUBSRCIPTIONS ONLY TO PREPROCESSING   
        sub_surf = nh.subscribe<sensor_msgs::PointCloud2>("/surf_features", 900, &LidarOdometry::laserCloudLessFlatHandler, this);
        sub_full_cloud = nh.subscribe<sensor_msgs::PointCloud2>("/lidar_cloud_uncorrected", 900, &LidarOdometry::FullPointCloudHandler, this);

        //RESULT PUBLISHER  
        pub_full_cloud = nh.advertise<sensor_msgs::PointCloud2>("/corrected_dynamic_points", 900);

        //DEBUGGING PUBLISHERS
        pub_surf = nh.advertise<sensor_msgs::PointCloud2>("/laser_cloud_surf_last", 900);
        pub_lc_map=nh.advertise<sensor_msgs::PointCloud2>("/lc_map_odom",900);
        pub_matched_pts=nh.advertise<sensor_msgs::PointCloud2>("/matched_pts",900);
        pub_trashed_pts=nh.advertise<sensor_msgs::PointCloud2>("/trashed_pts",900);
    }

    ~LidarOdometry(){}

    void allocateMemory() {
        surf_features.reset(new pcl::PointCloud<PointType>());
        full_cloud.reset(new pcl::PointCloud<PointType>());

        kd_tree_surf_last.reset(new pcl::KdTreeFLANN<PointType>());

        surf_from_map.reset(new pcl::PointCloud<PointType>());
        surf_from_map_ds.reset(new pcl::PointCloud<PointType>());
        surf_last_ds.reset(new pcl::PointCloud<PointType>());

        surf_current_pts.reset(new pcl::PointCloud<PointType>());
        surf_normal.reset(new pcl::PointCloud<PointType>());
    }

    void initializeParameters() {
        // Load parameters from yaml
        if (!getParameter("/common/frame_id", frame_id)) {
            ROS_WARN("frame_id not set, use default value: lili_om");
            frame_id = "lili_om";
        }

        if (!getParameter("/lidar_odometry/max_num_iter", max_num_iter)) {
            ROS_WARN("maximal iteration number not set, use default value: 15");
            max_num_iter = 15;
        }

        if (!getParameter("/lidar_odometry/scan_match_cnt", scan_match_cnt)) {
            ROS_WARN("number of scan matching not set, use default value: 1");
            scan_match_cnt = 1;
        }

        if(!getParameter("/lidar_odometry/weight_treshold",weight_treshold)){
            ROS_WARN("Weight treshold not set, use default: 2.5");
            weight_treshold=2.5;
        }

        if(!getParameter("/lidar_odometry/plane_treshold",plane_treshold)){
            ROS_WARN("Weight not set, use default: 0.06");
            plane_treshold=0.06;
        }

        if(!getParameter("/lidar_odometry/map_frames_cnt",map_frames_cnt)){
            ROS_WARN("map_frames_cnt not set, use default: 1");
            map_frames_cnt=1;
        }

        if(!getParameter("/lidar_odometry/max_time_limit",solver_time_limit)){
            ROS_WARN("max_time_limit  not set, use default: 0.015");
            solver_time_limit=0.015;
        }

        if(!getParameter("/common/if_to_debug",if_to_debug)){
            ROS_WARN("if_to_debug  not set, use default: false");
            if_to_debug=false;
        }

        if(!getParameter("/lidar_odometry/nn_dist_tresh",nn_dist_tresh)){
            ROS_WARN("nn_dist_tresh treshold not set, use default : 1.0");
            nn_dist_tresh=1.0;
        }

        system_initialized = false;

        rel_pose[0] = 1;
        for (int i = 1; i < 7; ++i) {
            rel_pose[i] = 0;
        }
        surf_res_cnt = 0;
    }

    void laserCloudLessFlatHandler(const sensor_msgs::PointCloud2ConstPtr &pointCloudIn) {
        //callback registering incoming surface points point cloud
        pcl::PointCloud<PointType> new_pcl;
        pcl::fromROSMsg(*pointCloudIn, new_pcl);
        new_surfs.push_back(new_pcl.makeShared());
    }

    void FullPointCloudHandler(const sensor_msgs::PointCloud2ConstPtr &pointCloudIn) {
        //callback registering incoming cutted point cloud
        cloud_header = pointCloudIn->header;
        pcl::PointCloud<PointType> new_pcl;
        pcl::fromROSMsg(*pointCloudIn, new_pcl);
        new_full_clouds.push_back(new_pcl.makeShared());
    }

    void undistortion(const pcl::PointCloud<PointType>::Ptr &pcloud, const Eigen::Vector3d trans) {
        double dt = 0.1; //frame duration
        for (auto &pt : pcloud->points) {
            double dt_i = double(pt.intensity);
            double ratio_i = dt_i / dt; 

            Eigen::Vector3d t_si =ratio_i * trans; // not-oriented total translation vector
            Eigen::Vector3d pt_i(pt.x, pt.y, pt.z);
            Eigen::Vector3d pt_s = pt_i + t_si;//fixed point

            //save fixed point to point cloud
            pt.x = pt_s.x();
            pt.y = pt_s.y();
            pt.z = pt_s.z();
        }
    }

    void checkInitialization() {
        sensor_msgs::PointCloud2 msgs;

        pcl::toROSMsg(*surf_features, msgs);
        msgs.header.stamp = cloud_header.stamp;
        msgs.header.frame_id = frame_id;
        pub_surf.publish(msgs);

        pcl::toROSMsg(*full_cloud, msgs);
        msgs.header.stamp = cloud_header.stamp;
        msgs.header.frame_id = frame_id;
        pub_full_cloud.publish(msgs);

        system_initialized = true;
    }

    void transformPoint(PointType const *const pi, PointType *const po) {
        Eigen::Quaterniond quaternion(rel_pose[0],
                rel_pose[1],
                rel_pose[2],
                rel_pose[3]);
        Eigen::Vector3d transition(rel_pose[4],
                rel_pose[5],
                rel_pose[6]);

        Eigen::Vector3d ptIn(pi->x, pi->y, pi->z);
        Eigen::Vector3d ptOut = quaternion * ptIn + transition;

        Eigen::Vector3d normIn(pi->normal_x, pi->normal_y, pi->normal_z);
        Eigen::Vector3d normOut = quaternion * normIn;

        po->x = ptOut.x();
        po->y = ptOut.y();
        po->z = ptOut.z();
        po->intensity = pi->intensity;
        po->curvature = pi->curvature;
        po->normal_x = normOut.x();
        po->normal_y = normOut.y();
        po->normal_z = normOut.z();
    }


    void buildLocalDynamicMap() {
        surf_from_map->clear();
        if (recent_surf_frames.size() < map_frames_cnt){  // prev 20
            recent_surf_frames.push_back(surf_frames[latest_frame_idx]);

        } else {
            //pop front and pushback to maintain 20 transformed and buffered frames
            if (latest_frame_idx != processed_frames - 1) {
                recent_surf_frames.pop_front();
                latest_frame_idx = processed_frames - 1;
                recent_surf_frames.push_back(surf_frames[latest_frame_idx]);
            }
        }
        //add all transformed points into local map
        for (int i = 0; i < recent_surf_frames.size(); ++i)
            *surf_from_map += *recent_surf_frames[i];


        if(if_to_debug){
            //publish local map:
            sensor_msgs::PointCloud2 temp_msg;
            pcl::PointCloud<PointXYZINormal> temp;
            pcl::copyPointCloud(*surf_from_map,temp);
            temp+=(*surf_features);
            pcl::toROSMsg(temp,temp_msg);
            temp_msg.header.stamp=cloud_header.stamp;
            temp_msg.header.frame_id=frame_id;
            pub_lc_map.publish(temp_msg);   
        }
    }

    void printEulerAngles(Eigen::Quaterniond quat){
        double yaw;
        double pitch;
        double roll;
        Eigen::Vector3d euler = quat.toRotationMatrix().eulerAngles(2, 1, 0);
        yaw = euler[0]; pitch = euler[1]; roll = euler[2];
        ROS_INFO("Computed additional rotation ... roll: %lf | pitch: %lf | yaw: %lf",roll, pitch, yaw);
    }

    void clearCloud() {
        surf_from_map->clear();
        surf_from_map_ds->clear();
        surf_features->clear();
        full_cloud->clear();
        if(surf_frames.size() > 7)
            surf_frames[surf_frames.size() - 8]->clear();
    }

    void copyClouds() {
        //downsamples last surfaces map and new incoming surface points cloud
        pcl::copyPointCloud(*surf_from_map,*surf_from_map_ds);
        pcl::copyPointCloud(*surf_features,*surf_last_ds);
    }

    void savePosesFeatures(){

        pcl::PointCloud<PointType>::Ptr surfEachFrame(new pcl::PointCloud<PointType>());

        pcl::copyPointCloud(*surf_last_ds, *surfEachFrame);

        surf_frames.push_back(surfEachFrame);
        processed_frames++;
    }
    void findCorrespondingSurfFeatures() {
        surf_res_cnt = 0;
        pcl::PointCloud<PointType> trashed_pts;
        const int neighbours_count=5;
        for (int i = 0; i < surf_last_ds->points.size(); ++i) {            
            PointType point_sel;
            transformPoint(&surf_last_ds->points[i], &point_sel);
            vector<int> point_search_idx; //nearest neighbours indices
            vector<float> point_search_dists; //nearest neighbours distances respectively
            kd_tree_surf_last->nearestKSearch(point_sel, neighbours_count, point_search_idx, point_search_dists); //find nearest 5 surface feature points

            Eigen::Matrix<double, neighbours_count, 3> matA0; //overdetermined system's variables matrix
            Eigen::Matrix<double, neighbours_count, 1> matB0 = - Eigen::Matrix<double, neighbours_count, 1>::Ones(); //overdetermined system's result vector

            if (point_search_dists[neighbours_count-1] < nn_dist_tresh) { // -- 1.0
                //largest value must be smaller than treshold 1
                PointType center; //center of mass of the plane

                for (int j = 0; j < neighbours_count; ++j) {
                    //fill the overdetermined system's matrix
                    matA0(j, 0) = surf_from_map_ds->points[point_search_idx[j]].x;
                    matA0(j, 1) = surf_from_map_ds->points[point_search_idx[j]].y;
                    matA0(j, 2) = surf_from_map_ds->points[point_search_idx[j]].z;
                }

                // Get the norm of the plane using linear solver based on QR composition
                Eigen::Vector3d norm = matA0.colPivHouseholderQr().solve(matB0); //gets normal vector of a plane fitted to 5 nearest points
                double normInverse = 1 / norm.norm();
                norm.normalize();

                // Compute the centroid of the plane
                center.x = matA0.col(0).sum() / double(neighbours_count);
                center.y = matA0.col(1).sum() / double(neighbours_count);
                center.z = matA0.col(2).sum() / double(neighbours_count);

                // Make sure that the plan is fit using point-to-plane metric and parameter treshold
                bool planeValid = true;
                for (int j = 0; j < neighbours_count; ++j) {
                    if (fabs(norm.x() * surf_from_map_ds->points[point_search_idx[j]].x +
                             norm.y() * surf_from_map_ds->points[point_search_idx[j]].y +
                             norm.z() * surf_from_map_ds->points[point_search_idx[j]].z + normInverse) > plane_treshold) {
                        planeValid = false;
                        break;
                    }
                }

                if (planeValid) {
                    float pd = norm.x() * point_sel.x + norm.y() * point_sel.y + norm.z() * point_sel.z + normInverse; //point to plane metric
                    float weight = 1 - 0.9 * fabs(pd) / sqrt(sqrt(point_sel.x * point_sel.x + point_sel.y * point_sel.y + point_sel.z * point_sel.z));

                    if(weight < weight_treshold) {
                        //save point and point representing matched plane
                        PointType normal;
                        normal.x = weight * norm.x();
                        normal.y = weight * norm.y();
                        normal.z = weight * norm.z();
                        normal.intensity = weight * normInverse;
                        surf_current_pts->push_back(surf_last_ds->points[i]);
                        surf_normal->push_back(normal);
                        ++surf_res_cnt;
                    }
                    else if(if_to_debug){
                        trashed_pts.push_back(surf_last_ds->points[i]);
                    }
                }
                else if(if_to_debug){
                    trashed_pts.push_back(surf_last_ds->points[i]);
                }
            }
        }
        //publish debugging pointclouds:
        if(if_to_debug){
            sensor_msgs::PointCloud2 temp_msg;
            pcl::toROSMsg(trashed_pts,temp_msg);
            temp_msg.header.stamp=cloud_header.stamp;
            temp_msg.header.frame_id=frame_id;
            pub_trashed_pts.publish(temp_msg);
            trashed_pts.clear();

            pcl::toROSMsg(*surf_current_pts,temp_msg);
            temp_msg.header.stamp=cloud_header.stamp;
            temp_msg.header.frame_id=frame_id;
            pub_matched_pts.publish(temp_msg);
        }
    }


    void updateTransformationWithCeres() {

        //reset current and relative pose
        double transformInc[7] = {1,0,0,0,0,0,0};
        Eigen::Quaterniond quat=Eigen::Quaterniond::Identity();
        rel_pose[0]=quat.w();
        rel_pose[1]=quat.x();
        rel_pose[2]=quat.y();
        rel_pose[3]=quat.z();
        rel_pose[4]=0;
        rel_pose[5]=0;
        rel_pose[6]=0;

        if (surf_from_map_ds->points.size() < 10) {
            ROS_WARN("Not enough feature points from the map");
            return;
        }
        kd_tree_surf_last->setInputCloud(surf_from_map_ds); //setup NN search for finding corresponding features

        for (int iter_cnt = 0; iter_cnt < scan_match_cnt; iter_cnt++) {
            ceres::LossFunction *lossFunction = new ceres::HuberLoss(0.1);
            ceres::LocalParameterization *quatParameterization = new ceres:: QuaternionParameterization();
            ceres::Problem problem;
            problem.AddParameterBlock(transformInc, 4, quatParameterization);
            problem.AddParameterBlock(transformInc + 4, 3);

            findCorrespondingSurfFeatures(); //measure point to plane metric with 5 nearest points

            for (int i = 0; i < surf_res_cnt; ++i) {
                Eigen::Vector3d currentPt(surf_current_pts->points[i].x,
                                          surf_current_pts->points[i].y,
                                          surf_current_pts->points[i].z); //matched point
                Eigen::Vector3d norm(surf_normal->points[i].x,
                                     surf_normal->points[i].y,
                                     surf_normal->points[i].z); //normal vector of feature point's plane
                double normInverse = surf_normal->points[i].intensity;

                ceres::CostFunction *costFunction = LidarPlaneNormIncreFactor::Create(currentPt, norm, normInverse);
                problem.AddResidualBlock(costFunction, lossFunction, transformInc, transformInc + 4);
            }

            ceres::Solver::Options solverOptions;
            solverOptions.linear_solver_type = ceres::DENSE_QR;
            solverOptions.max_num_iterations = max_num_iter;
            solverOptions.max_solver_time_in_seconds = solver_time_limit;
            solverOptions.minimizer_progress_to_stdout = false;
            solverOptions.check_gradients = false;
            solverOptions.gradient_check_relative_precision = 1e-2;

            ceres::Solver::Summary summary;
            ceres::Solve( solverOptions, &problem, &summary );

            if(transformInc[0] < 0) {
                //get the unit quaternion -- rotation only 
                Eigen::Quaterniond tmpQ(transformInc[0],
                        transformInc[1],
                        transformInc[2],
                        transformInc[3]);
                tmpQ = unifyQuaternion(tmpQ);
                transformInc[0] = tmpQ.w();
                transformInc[1] = tmpQ.x();
                transformInc[2] = tmpQ.y();
                transformInc[3] = tmpQ.z();
            }

            surf_current_pts->clear();
            surf_normal->clear();

            //update relative pose for finding coresponding surface features
            rel_pose[0] = transformInc[0];
            rel_pose[1] = transformInc[1];
            rel_pose[2] = transformInc[2];
            rel_pose[3] = transformInc[3];
            rel_pose[4] = transformInc[4];
            rel_pose[5] = transformInc[5];
            rel_pose[6] = transformInc[6];
        }

    }

    void publishCloudLast() {
        Eigen::Vector3d trans(rel_pose[4],rel_pose[5],rel_pose[6]);

        if(if_to_debug){
            //print calculated pose
            ROS_INFO("Computed additional translation ... %lf | %lf | %lf",rel_pose[4],rel_pose[5],rel_pose[6]);
            printEulerAngles(Eigen::Quaterniond(rel_pose[0],rel_pose[1],rel_pose[2],rel_pose[3]));
        }

        //DESKEW DYNAMIC POINTS
        undistortion(full_cloud,trans);

        sensor_msgs::PointCloud2 msg;
        pcl::toROSMsg(*full_cloud,msg);
        msg.header.stamp = cloud_header.stamp;
        msg.header.frame_id = frame_id;
        pub_full_cloud.publish(msg);
    }



    void run() {
        if (new_full_clouds.size()>=1 && new_surfs.size()>=1) {

            full_cloud=new_full_clouds.front();
            surf_features=new_surfs.front();

            Timer t_odm("LidarOdometry");
            if (!system_initialized) {
                //initialize global variables and end -- no more data to process
                savePosesFeatures();
                checkInitialization();
                return;
            }
            buildLocalDynamicMap(); //merge given number of frames into map - to match to
            copyClouds();
            updateTransformationWithCeres(); //iteratively minimize the distance based on point-to-plane metric
            savePosesFeatures();
            publishCloudLast(); //deskew and publish pointcloud
            clearCloud();
        
            new_surfs.pop_front();
            new_full_clouds.pop_front();

            if(if_to_debug){
                cout<<"odom_pub_cnt: "<<++odom_pub_cnt<<endl;
                t_odm.tic_toc();
                runtime += t_odm.toc();
                cout<<"Odometry average run time: "<<runtime / odom_pub_cnt<<endl;
            }
        }
    }
};

int main(int argc, char** argv) {
    google::InitGoogleLogging(argv[0]);
    ros::init(argc, argv, "lili_om");

    LidarOdometry LO;

    ROS_INFO("\033[1;32m---->\033[0m Lidar Odometry Started.");

    ros::Rate rate(250);

    while (ros::ok()) {
        ros::spinOnce();
        LO.run();
        rate.sleep();
    }

    ros::spin();
    return 0;
}
