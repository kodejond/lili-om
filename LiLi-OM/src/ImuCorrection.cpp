#include <utils/common.h>

#define LIDAR1_CODE "/livox/imu_1PQDH7600110791" 
#define LIDAR2_CODE "/livox/imu_3WEDJ5B00100371"
#define LIDAR3_CODE "/livox/imu_3WEDJ5B00100491"

#define PUBLISH_CODE "/livox/imu"
auto less_than=[](sensor_msgs::ImuConstPtr left, sensor_msgs::ImuConstPtr right){return left->header.stamp.toSec()<right->header.stamp.toSec();};

class ImuCorrector
{
    private:
        ros::NodeHandle node_hadle;
        ros::Publisher publisher;
        ros::Subscriber subscriber;
        Eigen::Matrix3d orientation_rotation;
        string sub_path;
        std::priority_queue<sensor_msgs::ImuConstPtr,vector<sensor_msgs::ImuConstPtr>,decltype(less_than)> *buffer;
        int id;
    public:

        Eigen::Matrix3d getRotationMatrix(const double pitch,const double roll, const double yaw){
            Eigen::AngleAxisd roll_angle(roll,Eigen::Vector3d::UnitX());
            Eigen::AngleAxisd pitch_angle(pitch,Eigen::Vector3d::UnitY());
            Eigen::AngleAxisd yaw_angle(yaw,Eigen::Vector3d::UnitZ());

            Eigen::Quaterniond temp_q=yaw_angle*pitch_angle*roll_angle;
            Eigen::Matrix3d result_rotation=temp_q.matrix();
            return result_rotation;
        }


        void ImuHandler(const sensor_msgs::ImuConstPtr& msg_in){
            Eigen::Vector3d linear_acceleration;
            Eigen::Vector3d angular_velocity;
            //fill temporary vector
            linear_acceleration[0]=msg_in->linear_acceleration.x;
            linear_acceleration[1]=msg_in->linear_acceleration.y;
            linear_acceleration[2]=msg_in->linear_acceleration.z;

            angular_velocity[0]=msg_in->angular_velocity.x;
            angular_velocity[1]=msg_in->angular_velocity.y;
            angular_velocity[2]=msg_in->angular_velocity.z;
            //rotate vectors:
            linear_acceleration=orientation_rotation*linear_acceleration;
            angular_velocity=orientation_rotation*angular_velocity;

            sensor_msgs::Imu *msg_out=new sensor_msgs::Imu();
            msg_out->header=msg_in->header;
            msg_out->angular_velocity.x=angular_velocity[0];
            msg_out->angular_velocity.y=angular_velocity[1];
            msg_out->angular_velocity.z=angular_velocity[2];
            msg_out->linear_acceleration.x=linear_acceleration[0];
            msg_out->linear_acceleration.y=linear_acceleration[1];
            msg_out->linear_acceleration.z=linear_acceleration[2];
            buffer->push(sensor_msgs::ImuConstPtr(msg_out));
        }

        ImuCorrector(ros::NodeHandle &nh, ros::Publisher pb, string subscription_path,const double pitch, const double yaw, const double roll,int id_,
            std::priority_queue<sensor_msgs::ImuConstPtr,vector<sensor_msgs::ImuConstPtr>,decltype(less_than)> *buf){
            node_hadle=nh;
            publisher=pb;
            orientation_rotation=getRotationMatrix(pitch,roll,yaw);
            sub_path=subscription_path;
            id=id_;
            buffer=buf;
            subscriber=node_hadle.subscribe<sensor_msgs::Imu>(sub_path,800,&ImuCorrector::ImuHandler,this);
            ROS_INFO("IMU corrector instance --- %d --- started",id);
        }
};

int main(int argc, char **argv){
    ros::init(argc,argv,"lili-om");
    ros::NodeHandle nh("~");
    ros::Publisher imu_pub=nh.advertise<sensor_msgs::Imu>(PUBLISH_CODE,800);
    ROS_INFO("\033[1;32m---->\033[0m IMU correction Started.");

    std::priority_queue<sensor_msgs::ImuConstPtr,vector<sensor_msgs::ImuConstPtr>,decltype(less_than)> imu_buf(less_than);    

    ImuCorrector horizon_r(nh,imu_pub,LIDAR3_CODE,0.0,-25.600000381469727,0.0,2,&imu_buf);
    ImuCorrector horizon_l(nh,imu_pub,LIDAR2_CODE,0.0,25.600000381469727,0.0,1,&imu_buf);    

    ROS_INFO("Starting processing callbacks");
   
    ros::Rate rate(600);
    sensor_msgs::Imu msg;
    sensor_msgs::ImuConstPtr msg_ptr;
    while(ros::ok()){
        ros::spinOnce();
        if(imu_buf.size()>=5){
            msg_ptr=imu_buf.top();
            msg.header=msg_ptr->header;
            msg.angular_velocity=msg_ptr->angular_velocity;
            msg.linear_acceleration=msg_ptr->linear_acceleration;
            imu_pub.publish(msg);
            imu_buf.pop();
        }
        rate.sleep();
    }
    return EXIT_SUCCESS;
}










