#include "utils/common.h"
#include "utils/timer.h"
#include "utils/math_tools.h"

class Earlier{
    public:
        bool operator()(sensor_msgs::PointCloud2 first,sensor_msgs::PointCloud2 second){
            return first.header.stamp.toSec() > second.header.stamp.toSec();
        }
};

class Preprocessing {
private:
    ros::NodeHandle nh;

    ros::Subscriber sub_Lidar_cloud;

    ros::Publisher pub_surf;
    ros::Publisher pub_cutted_cloud;
    ros::Publisher pub_static_cloud;

    int pre_num = 0;

    pcl::PointCloud<PointXYZINormal> lidar_cloud_in;
    std_msgs::Header cloud_header;

    std::priority_queue<sensor_msgs::PointCloud2,std::vector<sensor_msgs::PointCloud2>,Earlier> cloud_queue;

    sensor_msgs::PointCloud2 current_cloud_msg;

    int N_SCANS = 6; //number of laser lines = 6
    int H_SCANS = 4000;

    string frame_id = "lili_om";
    double surf_thres;
    double runtime = 0;
    
    std::deque<pcl::PointCloud<PointXYZINormal>::Ptr> surf_buffer;
    std::deque<pcl::PointCloud<PointXYZINormal>::Ptr> pcl_buffer;
    std::deque<pcl::PointCloud<PointXYZINormal>::Ptr> static_buffer; 
    std::deque<double> pcl_times;

    float surf_ds;
    pcl::VoxelGrid<PointXYZINormal> surf_filter;

    double z_ground_span;
    double ground_norm_tresh;

    bool if_to_debug;


pcl::PointCloud<PointXYZINormal> *mergePointClouds(pcl::PointCloud<PointXYZINormal>::Ptr cloud_start,
                                                        pcl::PointCloud<PointXYZINormal>::Ptr cloud_middle,
                                                        pcl::PointCloud<PointXYZINormal>::Ptr cloud_end, double t1, double t2, double t3){
        pcl::PointCloud<PointXYZINormal> *result=new pcl::PointCloud<PointXYZINormal>();
        double time_delta1=t2-t1;
        double time_delta2=t3-t2;
        if(time_delta1>=0.03 || time_delta2>=0.03){
            //lidar cloud out of sync
            ROS_WARN("Preprocessing time deltas ---- %lf , %lf",time_delta1,time_delta2);
        }
        pcl::copyPointCloud(*cloud_start,*result);
        *result+=(*cloud_middle);
        *result+=(*cloud_end);
        return result;
    }


public:
    Preprocessing(): nh("~") {

        if (!getParameter("/preprocessing/surf_thres", surf_thres))
        {
            ROS_WARN("surf_thres not set, use default value: 0.2");
            surf_thres = 0.2;
        }

        if (!getParameter("/common/frame_id", frame_id))
        {
            ROS_WARN("frame_id not set, use default value: lili_om");
            frame_id = "lili_om";
        }

        if (!getParameter("/lidar_odometry/surf_ds", surf_ds))
        {
            ROS_WARN("surf_ds not set, use default value: 0.3");
            surf_ds=0.3;
        }

        if (!getParameter("/preprocessing/z_ground_span", z_ground_span))
        {
            ROS_WARN("z_ground_span not set, use default value: 1");
            z_ground_span=1;
        }

        if (!getParameter("/preprocessing/ground_norm_tresh", ground_norm_tresh))
        {
            ROS_WARN("ground_norm_tresh not set, use default value: 0.9");
            ground_norm_tresh=0.9;
        }

        if(!getParameter("common/if_to_debug",if_to_debug)){
            ROS_WARN("if_to_debug  not set, use default: false");
            if_to_debug=false;
        }

        sub_Lidar_cloud = nh.subscribe<sensor_msgs::PointCloud2>("/points", 800, &Preprocessing::cloudHandler, this);

        pub_surf = nh.advertise<sensor_msgs::PointCloud2>("/surf_features", 200);
        pub_cutted_cloud = nh.advertise<sensor_msgs::PointCloud2>("/lidar_cloud_uncorrected", 200);
        pub_static_cloud = nh.advertise<sensor_msgs::PointCloud2>("/lidar_cloud_static",200);

    }

    ~Preprocessing(){}

    template <typename PointT>
    void removeClosedPointCloud(const pcl::PointCloud<PointT> &cloud_in, pcl::PointCloud<PointT> &cloud_out, float thres) {
        //cuts out points that are closer to the lidar than given treshold
        if (&cloud_in != &cloud_out) {
            cloud_out.header = cloud_in.header;
            cloud_out.points.resize(cloud_in.points.size());
        }

        size_t j = 0;

        for (size_t i = 0; i < cloud_in.points.size(); ++i) {
            if (cloud_in.points[i].x * cloud_in.points[i].x +
                    cloud_in.points[i].y * cloud_in.points[i].y +
                    cloud_in.points[i].z * cloud_in.points[i].z < thres * thres)
                continue;
            cloud_out.points[j] = cloud_in.points[i];
            j++;
        }
        if (j != cloud_in.points.size()) {
            cloud_out.points.resize(j);
        }

        cloud_out.height = 1;
        cloud_out.width = static_cast<uint32_t>(j);
        cloud_out.is_dense = true;
    }
    
    template <typename PointT>
    double getDepth(PointT pt) {
        //computes distance of given point from lidar
        return sqrt(pt.x*pt.x + pt.y*pt.y + pt.z*pt.z);
    }


    void cloudHandler( const sensor_msgs::PointCloud2ConstPtr &lidar_cloud_msg) {
        cloud_queue.push(*lidar_cloud_msg);
        if (cloud_queue.size() < 4){
            return;
        }   
        else
        {
            current_cloud_msg = cloud_queue.top();
            cloud_queue.pop();

            cloud_header = current_cloud_msg.header;
            cloud_header.frame_id = frame_id;
        }

        
        Timer t_pre("Preprocessing");
        pcl::fromROSMsg(current_cloud_msg, lidar_cloud_in);

        std::vector<int> indices;

        //filter point cloud from Nan values and too near points
        pcl::removeNaNFromPointCloud(lidar_cloud_in, lidar_cloud_in, indices);
        removeClosedPointCloud(lidar_cloud_in, lidar_cloud_in, 0.1);

        int cloud_size = lidar_cloud_in.points.size();

        PointXYZINormal point; //raw point cursor
        PointXYZINormal mat[N_SCANS][H_SCANS]; //one scan sweep matrix
        pcl::PointCloud<PointXYZINormal>::Ptr dynamic_points(new pcl::PointCloud<PointXYZINormal>());
        pcl::PointCloud<PointXYZINormal>::Ptr static_points(new pcl::PointCloud<PointXYZINormal>());
        double t_interval = 0.1 / (H_SCANS-1); //interval between measuring each point -- 0.1 = frame time
        pcl::PointCloud<PointXYZINormal>::Ptr surf_features(new pcl::PointCloud<PointXYZINormal>()); //stores extracted surface feature points
        for (int i = 0; i < cloud_size; i++) {
            point.x=lidar_cloud_in.points[i].x;
            point.y=lidar_cloud_in.points[i].y;
            point.z=lidar_cloud_in.points[i].z;
            point.intensity=lidar_cloud_in.points[i].intensity;
            point.curvature=lidar_cloud_in.points[i].curvature;
            point.normal_x=lidar_cloud_in.points[i].normal_x;
            point.normal_z=lidar_cloud_in.points[i].normal_z; // dynamic
            point.normal_y=lidar_cloud_in.points[i].normal_y;

            if(lidar_cloud_in.points[i].normal_z==1){
                dynamic_points->push_back(lidar_cloud_in.points[i]);
            }
            else{
                static_points->push_back(lidar_cloud_in.points[i]);
            }   

            int scan_id = 0;
            if (N_SCANS == 6)
                scan_id = (int)point.normal_y;
            if(scan_id < 0) //skip inconcrete readings idk
                continue;

            double dep = point.x*point.x + point.y*point.y + point.z*point.z; //squared depth of the point
            if(dep > 40000.0 || dep < 4.0 || point.curvature < 0.05 || point.curvature > 25.45)
                continue; //skip inconcrete point readings or already processed points
            int col = int(round(double(point.intensity) / t_interval));  // 1.dim = laser_line | 2.dim = time within frame
            if (col >= H_SCANS || col < 0)
                continue;
            if (mat[scan_id][col].curvature != 0)
                continue;
            mat[scan_id][col] = point; //save for edge feature extraction??? -> use intensity to determine curvature
        }

        
        for(int i = 5; i < H_SCANS - 12; i = i + 6) { //get valid points for surface features computing
            vector<Eigen::Vector3d> near_pts;
            Eigen::Vector3d center(0, 0, 0);
            int num = 36;
            for(int j = 0; j < 6; j++) { //compute center of mass
                for(int k = 0; k < N_SCANS; k++) {
                    if(mat[k][i+j].curvature <= 0) {
                        num--;
                        continue;
                    }
                    Eigen::Vector3d pt(mat[k][i+j].x,
                            mat[k][i+j].y,
                            mat[k][i+j].z);
                    center += pt;
                    near_pts.push_back(pt);
                }
            }
            if(num < 19)
                continue;
            center /= num; //finish computing center of mass


            // Covariance matrix
            Eigen::Matrix3d matA1 = Eigen::Matrix3d::Zero(); //surface feature covariance matrix
            for (int j = 0; j < near_pts.size(); j++)
            {
                Eigen::Vector3d zero_mean = near_pts[j] - center;
                matA1 += (zero_mean * zero_mean.transpose());
            }

            Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen_solver(matA1);

            if(eigen_solver.eigenvalues()[0] < surf_thres * eigen_solver.eigenvalues()[1]) {
                Eigen::Vector3d unitDirection = eigen_solver.eigenvectors().col(0); //plane normal vector -- encode as normal vector of each feature point
                for(int j = 0; j < 6; j++) {
                    for(int k = 0; k < N_SCANS; k++) {
                        if(mat[k][i+j].curvature <= 0 || mat[k][i+j].normal_z==0) {
                            //skip already visited and static points
                            continue;
                        }

                        mat[k][i+j].normal_x = unitDirection.x();
                        mat[k][i+j].normal_y = unitDirection.y();
                        mat[k][i+j].normal_z = unitDirection.z();

                        surf_features->points.push_back(mat[k][i+j]);
                        mat[k][i+j].curvature *= -1; //skip in other iterations
                    }
                }
            }
        }

        if(pcl_buffer.size()==2 && surf_buffer.size()==2){
            pcl::PointCloud<PointXYZINormal> *surfs=mergePointClouds(surf_buffer[0],surf_buffer[1],surf_features,pcl_times[0],pcl_times[1],cloud_header.stamp.toSec());
            surf_filter.setInputCloud((*surfs).makeShared());
            pcl::PointCloud<PointXYZINormal> final_surfs;
            surf_filter.setLeafSize(surf_ds,surf_ds,surf_ds);
            surf_filter.filter(final_surfs);
            pcl::PointCloud<PointXYZINormal> *pcls=mergePointClouds(pcl_buffer[0],pcl_buffer[1],dynamic_points,pcl_times[0],pcl_times[1],cloud_header.stamp.toSec());
            pcl::PointCloud<PointXYZINormal> *statics=mergePointClouds(static_buffer[0],static_buffer[1],static_points,pcl_times[0],pcl_times[1],cloud_header.stamp.toSec());


            for(int i=0;i<2;++i){
                pcl_buffer.pop_front();
                surf_buffer.pop_front();
                static_buffer.pop_front();
                pcl_times.pop_front();
            }
            
            //PUBLISH SURFACE FEATURES
            sensor_msgs::PointCloud2 surf_features_msg;
            pcl::toROSMsg(final_surfs,surf_features_msg);
            surf_features_msg.header.stamp = cloud_header.stamp;
            surf_features_msg.header.frame_id = frame_id;
            pub_surf.publish(surf_features_msg);

            //PUBLISH FULL POINT CLOUD WITHOUT NEAR POINTS
            sensor_msgs::PointCloud2 cloud_cutted_msg;
            pcl::toROSMsg(*pcls, cloud_cutted_msg);
            cloud_cutted_msg.header.stamp = cloud_header.stamp;
            cloud_cutted_msg.header.frame_id = frame_id;
            pub_cutted_cloud.publish(cloud_cutted_msg);

            sensor_msgs::PointCloud2 static_points_msg;
            pcl::toROSMsg(*statics,static_points_msg);
            static_points_msg.header.stamp = cloud_header.stamp;
            static_points_msg.header.frame_id = frame_id;
            pub_static_cloud.publish(static_points_msg);

        }
        else{
            surf_buffer.push_back(surf_features);
            pcl_buffer.push_back(dynamic_points);
            static_buffer.push_back(static_points);
            pcl_times.push_back(cloud_header.stamp.toSec());
            
        }
        
        if(if_to_debug){
            t_pre.tic_toc();
            runtime += t_pre.toc();
            cout<<"pre_num: "<<++pre_num<<endl;
            cout<<"Preprocessing average run time: "<<runtime / pre_num<<endl;
        }
    }
};


int main(int argc, char** argv) {
    ros::init(argc, argv, "lili_om");

    Preprocessing Pre;

    ROS_INFO("\033[1;32m---->\033[0m Preprocessing Started.");
    ros::spin();
    return 0;
}
