#include "utils/common.h"
#include "utils/timer.h"
#include "utils/math_tools.h"

class Earlier{
    public:
        bool operator()(sensor_msgs::PointCloud2 first,sensor_msgs::PointCloud2 second){
            return first.header.stamp.toSec() > second.header.stamp.toSec();
        }
};

auto lower=[](PointType first, PointType second){
    return first.z<second.z;
};


class Preprocessing {
private:
    ros::NodeHandle nh;

    ros::Subscriber sub_Lidar_cloud;
    ros::Subscriber sub_imu;

    ros::Publisher pub_surf;
    ros::Publisher pub_full_cloud;


    int pre_num = 0;

    std_msgs::Header cloud_header;

    vector<sensor_msgs::ImuConstPtr> imu_buf; //buffer for incoming IMU messages
    int idx_imu = 0; //last processed IMU message's timestamp
    double current_time_imu = -1; //last processed IMU message's timestamp

    Eigen::Vector3d gyr_0; // previous angular velocity -- for computing the mean value
    Eigen::Quaterniond q_iMU = Eigen::Quaterniond::Identity();  // current orientation of IMU -- reset after each point cloud -> realative movement between two point clouds
    bool first_imu = false;

    std::priority_queue<sensor_msgs::PointCloud2,std::vector<sensor_msgs::PointCloud2>,Earlier> cloud_queue;

    sensor_msgs::PointCloud2 current_cloud_msg;
    double time_scan_next;

    int N_SCANS = 6;
    int H_SCANS = 4000;

    string frame_id = "lili_om";
    
    double runtime = 0;
    
    std::deque<pcl::PointCloud<PointType>::Ptr> surf_buffer;
    std::deque<pcl::PointCloud<PointType>::Ptr> pcl_buffer;
    std::deque<double> pcl_times;

    double z_ground_span;
    double ground_norm_tresh;
    bool if_to_debug;
    bool filter_ground;
    double surf_thres;

pcl::PointCloud<PointType> *mergePointClouds(pcl::PointCloud<PointType>::Ptr cloud_start,
                                                        pcl::PointCloud<PointType>::Ptr cloud_middle,
                                                        pcl::PointCloud<PointType>::Ptr cloud_end, double t1, double t2, double t3){
        pcl::PointCloud<PointType> *result=new pcl::PointCloud<PointType>();
        double time_delta1=t2-t1;
        double time_delta2=t3-t2;
        if(time_delta1>=0.03 || time_delta2>=0.03){
            //lidar cloud out of sync
            ROS_WARN("Egomotion Preprocessing time deltas ---- %lf , %lf",time_delta1,time_delta2);
        }
        pcl::copyPointCloud(*cloud_start,*result);
        for(size_t idx=0;idx<result->points.size();++idx){
            int line=(int)result->points[idx].intensity;
            float time_i=result->points[idx].intensity - float(line);
            result->points[idx].intensity=time_i;
        }
        for(size_t idx=0;idx<cloud_middle->points.size();++idx){
            int line=(int)cloud_middle->points[idx].intensity;
            float time_i=cloud_middle->points[idx].intensity-float(line);
            cloud_middle->points[idx].intensity=time_i;
        }
        *result+=(*cloud_middle);
        for(size_t idx=0;idx<cloud_end->points.size();++idx){
            int line=(int)cloud_end->points[idx].intensity;
            float time_i=cloud_end->points[idx].intensity-float(line);
            cloud_end->points[idx].intensity=time_i;
        }
        *result+=(*cloud_end);
        return result;
    }


public:
    Preprocessing(): nh("~") {

        if (!getParameter("/preprocessing/surf_thres", surf_thres))
        {
            ROS_WARN("surf_thres not set, use default value: 0.2");
            surf_thres = 0.2;
        }

        if (!getParameter("/common/frame_id", frame_id))
        {
            ROS_WARN("frame_id not set, use default value: lili_om");
            frame_id = "lili_om";
        }

        if (!getParameter("/preprocessing/z_ground_span", z_ground_span))
        {
            ROS_WARN("z_ground_span not set, use default value: 1");
            z_ground_span=1;
        }

        if (!getParameter("/preprocessing/ground_norm_tresh", ground_norm_tresh))
        {
            ROS_WARN("ground_norm_tresh not set, use default value: 0.9");
            ground_norm_tresh=0.9;
        }

        if(!getParameter("common/if_to_debug",if_to_debug)){
            ROS_WARN("if_to_debug  not set, use default: false");
            if_to_debug=false;
        }

        if(!getParameter("preprocessing/filter_ground",filter_ground)){
            ROS_WARN("filter_ground  not set, use default: false");
            filter_ground=false;
        }

        sub_Lidar_cloud = nh.subscribe<sensor_msgs::PointCloud2>("/livox_ros_points", 800, &Preprocessing::cloudHandler, this);
        sub_imu = nh.subscribe<sensor_msgs::Imu>("/livox/imu", 800, &Preprocessing::imuHandler, this);

        pub_surf = nh.advertise<sensor_msgs::PointCloud2>("/dynamic_surf_features", 800);
        pub_full_cloud = nh.advertise<sensor_msgs::PointCloud2>("/lidar_cloud_dynamic", 800);

    }

    ~Preprocessing(){}

    template <typename PointType>
    void removeClosedPointCloud(pcl::PointCloud<PointType> &cloud_in, pcl::PointCloud<PointType> &cloud_out, float thres) {
        //cuts out points that are closer to the lidar than given treshold
        if (&cloud_in != &cloud_out) {
            cloud_out.header = cloud_in.header;
            cloud_out.points.resize(cloud_in.points.size());
        }

        size_t j = 0;

        for (size_t i = 0; i < cloud_in.points.size(); ++i) {
            if (cloud_in.points[i].x * cloud_in.points[i].x +
                    cloud_in.points[i].y * cloud_in.points[i].y +
                    cloud_in.points[i].z * cloud_in.points[i].z < thres * thres)
                continue;
            cloud_out.points[j] = cloud_in.points[i];
            j++;
        }
        if (j != cloud_in.points.size()) {
            cloud_out.points.resize(j);
        }

        cloud_out.height = 1;
        cloud_out.width = static_cast<uint32_t>(j);
        cloud_out.is_dense = true;
    }
    
    template <typename PointT>
    double getDepth(PointT pt) {
        //computes distance of given point from lidar
        return sqrt(pt.x*pt.x + pt.y*pt.y + pt.z*pt.z);
    }

    void undistortion(PointType *pt, const Eigen::Quaterniond quat) {
        double dt = 0.1;
        int line = int(pt->intensity);
        double dt_i = pt->intensity - line; //float(livox_msg_in->points[i].offset_time / (float)time_end) -- relative time from the absolute start
        double ratio_i = dt_i / dt; 
        if(ratio_i >= 1.0) {
            ratio_i = 1.0;
        }

        Eigen::Quaterniond q0 = Eigen::Quaterniond::Identity();
        Eigen::Quaterniond q_si = q0.slerp(ratio_i, q_iMU); // get the transition quaternion from the absolute start to present -> current point's orientation

        Eigen::Vector3d pt_i(pt->x, pt->y, pt->z);
        Eigen::Vector3d pt_s = q_si * pt_i; //rotate point's coordinates

        //save corrected point:
        pt->x = pt_s.x();
        pt->y = pt_s.y();
        pt->z = pt_s.z();
    }

    void solveRotation(double dt, Eigen::Vector3d angular_velocity) {
        //updates current IMU orientation with given velocity over given time period dt
        Eigen::Vector3d un_gyr = 0.5 * (gyr_0 + angular_velocity); //use mean value of angular velocity -- better aproximation
        q_iMU *= deltaQ(un_gyr * dt);
        gyr_0 = angular_velocity; 
    }

    void removeGround(pcl::PointCloud<PointType> *cloud, pcl::PointCloud<PointType>::Ptr cloud_out){

        pcl::KdTreeFLANN<PointType> search_tree;
        search_tree.setInputCloud(cloud_out);

        std::sort(cloud->points.begin(),cloud->points.end(),lower);
        double min_z=cloud->points[0].z;

        size_t rm_cnt=0;
        size_t skip_cnt=0;

        for(size_t idx=0; idx<cloud->points.size();++idx){
            PointType cur_pt=cloud->points[idx];
            if((cur_pt.z - min_z) >z_ground_span){
                break;
            }
            else{
                Eigen::Vector3d cur_norm(cur_pt.normal_x,cur_pt.normal_y,cur_pt.normal_z);
                cur_norm.normalize();
                if(abs(cur_norm[2]) >= ground_norm_tresh){
                    //remove from cloud out:
                    std::vector<int> indices;
                    std::vector<float> distances;
                    search_tree.nearestKSearch(cur_pt,1,indices,distances);
                    //set ground point to STATIC:
                    cloud_out->points[indices[0]].normal_z=-1;
                    ++rm_cnt;
                }
                else{
                    ++skip_cnt;
                }
            }  
        }
        if(if_to_debug){
            ROS_INFO("Removed : %lu | Skipped: %lu out of %lu",rm_cnt,skip_cnt,cloud->size());
        }
        
    }

    void processIMU(double t_cur) {
        double rx = 0, ry = 0, rz = 0;
        int i = idx_imu;
        if(i >= imu_buf.size())
            i--;
        while(imu_buf[i]->header.stamp.toSec() < t_cur) {
            //processes IMU messages up to given time t_cur
            double t = imu_buf[i]->header.stamp.toSec(); //IMU message's timestamp
            if (current_time_imu < 0)
                current_time_imu = t;
            double dt = t - current_time_imu; //time_delta from previous IMU message

            current_time_imu = imu_buf[i]->header.stamp.toSec(); //update last message's timestamp
            //retrieve the angular velocity parameters
            rx = imu_buf[i]->angular_velocity.x;
            ry = imu_buf[i]->angular_velocity.y;
            rz = imu_buf[i]->angular_velocity.z;
            //update current global IMU orientation:
            solveRotation(dt, Eigen::Vector3d(rx, ry, rz));
            i++;
            if(i >= imu_buf.size())
                break;

        }

        //if more messages available
        if(i < imu_buf.size()) {
            double dt1 = t_cur - current_time_imu; //remaining time_delta of current message
            double dt2 = imu_buf[i]->header.stamp.toSec() - t_cur; //ramaning time_delta of next message

            double w1 = dt2 / (dt1 + dt2);
            double w2 = dt1 / (dt1 + dt2);

            //estimate combination of both neighbouring messages:
            rx = w1 * rx + w2 * imu_buf[i]->angular_velocity.x;
            ry = w1 * ry + w2 * imu_buf[i]->angular_velocity.y;
            rz = w1 * rz + w2 * imu_buf[i]->angular_velocity.z;
            solveRotation(dt1, Eigen::Vector3d(rx, ry, rz));
        }
        current_time_imu = t_cur;
        idx_imu = i;
    }

    void imuHandler(const sensor_msgs::ImuConstPtr& imu_in) {
        //callback buffering incoming IMU messages - inits variables during first run
        imu_buf.push_back(imu_in);
        if(imu_buf.size() > 9000)
            imu_buf[imu_buf.size() - 9001] = nullptr;

        if (current_time_imu < 0)
            current_time_imu = imu_in->header.stamp.toSec();

        if (!first_imu)
        {
            //first message -> init global variables
            first_imu = true;
            double rx = 0, ry = 0, rz = 0;
            rx = imu_in->angular_velocity.x;
            ry = imu_in->angular_velocity.y;
            rz = imu_in->angular_velocity.z;
            Eigen::Vector3d angular_velocity(rx, ry, rz);
            gyr_0 = angular_velocity;
        }
    }

    void undistort_frame(pcl::PointCloud<PointType>::Ptr pcl_distorted,const Eigen::Quaterniond quat){
        for(size_t idx=0;idx<pcl_distorted->points.size();++idx){
            PointType *pt=&(pcl_distorted->points[idx]);
            undistortion(pt,quat);
        }
    }

    void cloudHandler( const sensor_msgs::PointCloud2ConstPtr &lidar_cloud_msg) {
        // callback buffering incoming point clouds && automatically processed the buffer
        cloud_queue.push(*lidar_cloud_msg);
        if (cloud_queue.size() <= 5){
            return;
        }   
        else
        {
            current_cloud_msg = cloud_queue.top();
            cloud_queue.pop();

            cloud_header = current_cloud_msg.header;
            cloud_header.frame_id = frame_id;
            time_scan_next = cloud_queue.top().header.stamp.toSec();
        }

        int tmp_idx = 0;
        if(idx_imu > 0)
            tmp_idx = idx_imu - 1;
        //check for IMU data to de-skew incoming point cloud
        if ((imu_buf.empty() || imu_buf[tmp_idx]->header.stamp.toSec() > time_scan_next) && if_to_debug)
        {
            ROS_WARN("Missing IMU data ...");
        }

        Timer t_pre("Egomotion Preprocessing");
        pcl::PointCloud<PointType>::Ptr lidar_cloud(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(current_cloud_msg, *lidar_cloud);

        std::vector<int> indices;

        //filter point cloud from Nan values and too near points
        pcl::removeNaNFromPointCloud(*lidar_cloud,*lidar_cloud, indices);
        removeClosedPointCloud(*lidar_cloud,*lidar_cloud, 0.1);

        int cloud_size = lidar_cloud->points.size();
        
        Eigen::Quaterniond prev_q_IMU(q_iMU.w(),q_iMU.x(),q_iMU.y(),q_iMU.z());
        //update IMU orientation up to current time
        if(imu_buf.size() > 0)
            processIMU(time_scan_next);
        if(isnan(q_iMU.w()) || isnan(q_iMU.x()) || isnan(q_iMU.y()) || isnan(q_iMU.z())) {
            //reset IMU orientation if INVALID
            ROS_WARN("IMU orientation invalid(NaN) -> resetting to origin!!!");
            q_iMU=prev_q_IMU;
        }

        PointType point_undis; //IMU processed point cursor
        PointType mat[N_SCANS][H_SCANS]; //one scan sweep matrix
        double t_interval = 0.1 / (H_SCANS-1); //interval between measuring each point -- 0.1 = frame time
        pcl::PointCloud<PointType>::Ptr surf_features(new pcl::PointCloud<PointType>()); //stores extracted surface feature points

        undistort_frame(lidar_cloud,q_iMU);

        for (int i = 0; i < cloud_size; i++) {

            point_undis.x=lidar_cloud->points[i].x;
            point_undis.y=lidar_cloud->points[i].y;
            point_undis.z=lidar_cloud->points[i].z;
            point_undis.normal_x=lidar_cloud->points[i].normal_x;
            point_undis.normal_y=lidar_cloud->points[i].normal_y;
            point_undis.normal_z=lidar_cloud->points[i].normal_z;
            point_undis.curvature=lidar_cloud->points[i].curvature;
            point_undis.intensity=lidar_cloud->points[i].intensity;


            int scan_id = 0;
            if (N_SCANS == 6)
                scan_id = (int)point_undis.intensity;
            if(scan_id < 0) //skip inconcrete readings idk
                continue;

            double dep = point_undis.x*point_undis.x + point_undis.y*point_undis.y + point_undis.z*point_undis.z;
            if(dep > 40000.0 || dep < 4.0 || point_undis.curvature < 0.05 || point_undis.curvature > 25.45)
                continue;
            int col = int(round((point_undis.intensity - scan_id) / t_interval));  // 1.dim = laser_line | 2.dim = time within frame
            if (col >= H_SCANS || col < 0)
                continue;
            if (mat[scan_id][col].curvature != 0)
                continue;
            mat[scan_id][col] = point_undis; //save unfolded sweep
        }

        for(int i = 5; i < H_SCANS - 12; i = i + 6) {
            vector<Eigen::Vector3d> near_pts;
            Eigen::Vector3d center(0, 0, 0);
            int num = 36;
            for(int j = 0; j < 6; j++) { //compute center of mass
                for(int k = 0; k < N_SCANS; k++) {
                    if(mat[k][i+j].curvature <= 0) {
                        num--;
                        continue;
                    }
                    Eigen::Vector3d pt(mat[k][i+j].x,
                            mat[k][i+j].y,
                            mat[k][i+j].z);
                    center += pt;
                    near_pts.push_back(pt);
                }
            }
            if(num < 25)
                continue;
            center /= num; //finish computing center of mass

            // Covariance matrix
            Eigen::Matrix3d matA1 = Eigen::Matrix3d::Zero(); //surface feature covariance matrix
            for (int j = 0; j < near_pts.size(); j++)
            {
                Eigen::Vector3d zero_mean = near_pts[j] - center;
                matA1 += (zero_mean * zero_mean.transpose());
            }

            Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen_solver(matA1);

            if(eigen_solver.eigenvalues()[0] < surf_thres * eigen_solver.eigenvalues()[1]) {
                Eigen::Vector3d unitDirection = eigen_solver.eigenvectors().col(0); //plane normal vector -- encode as normal vector of each feature point
                for(int j = 0; j < 6; j++) {
                    for(int k = 0; k < N_SCANS; k++) {
                        if(mat[k][i+j].curvature <= 0) {
                            continue;
                        }
                        mat[k][i+j].normal_x = unitDirection.x();
                        mat[k][i+j].normal_y = unitDirection.y();
                        mat[k][i+j].normal_z = unitDirection.z();

                        surf_features->points.push_back(mat[k][i+j]);
                        mat[k][i+j].curvature *= -1; //skip in other iterations
                    }
                }
            }
        }

        if(filter_ground){
            removeGround(&(*surf_features),lidar_cloud);
        }

        if(pcl_buffer.size()==2 && surf_buffer.size()==2){
            pcl::PointCloud<PointType> *surfs=mergePointClouds(surf_buffer[0],surf_buffer[1],surf_features,pcl_times[0],pcl_times[1],cloud_header.stamp.toSec());
            pcl::PointCloud<PointType> final_surfs;
            pcl::PointCloud<PointType> *pcls=mergePointClouds(pcl_buffer[0],pcl_buffer[1],lidar_cloud,pcl_times[0],pcl_times[1],cloud_header.stamp.toSec());

            for(int i=0;i<2;++i){
                pcl_buffer.erase(pcl_buffer.begin());
                surf_buffer.erase(surf_buffer.begin());
                pcl_times.erase(pcl_times.begin());
            }
            
            if(if_to_debug){
                //PUBLISH SURFACE FEATURES FOR DEBUGGING
                sensor_msgs::PointCloud2 surf_features_msg;
                pcl::toROSMsg(final_surfs,surf_features_msg);
                surf_features_msg.header.stamp = cloud_header.stamp;
                surf_features_msg.header.frame_id = frame_id;
                pub_surf.publish(surf_features_msg);
            }

            //PUBLISH FULL POINT CLOUD WITHOUT NEAR POINTS
            sensor_msgs::PointCloud2 cloud_full_msg;
            pcl::toROSMsg(*pcls, cloud_full_msg);
            cloud_full_msg.header.stamp = cloud_header.stamp;
            cloud_full_msg.header.frame_id = frame_id;
            pub_full_cloud.publish(cloud_full_msg);

        }
        else{
            surf_buffer.push_back(surf_features);
            pcl_buffer.push_back(lidar_cloud);
            pcl_times.push_back(cloud_header.stamp.toSec());
        }
        
        q_iMU = Eigen::Quaterniond::Identity();
        if(if_to_debug){
            t_pre.tic_toc();
            runtime += t_pre.toc();
            cout<<"pre_num: "<<++pre_num<<endl;
            cout<<"Egomotion Preprocessing average run time: "<<runtime / pre_num<<endl;
        }
    }
};


int main(int argc, char** argv) {
    ros::init(argc, argv, "lili_om");

    Preprocessing Pre;

    ROS_INFO("\033[1;32m---->\033[0m Egomotion Preprocessing Started.");
    ros::spin();
    return 0;
}
