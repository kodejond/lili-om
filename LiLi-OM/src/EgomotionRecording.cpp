#include "utils/common.h"
#include "utils/timer.h"


class EgomotionRecorder{
    private:
        ros::NodeHandle nh;

        ros::Subscriber sub_full;
        ros::Subscriber sub_poses;

        ros::Publisher pub_frame;

        pcl::PointCloud<PointType>::Ptr cur_pcl;
        std::deque<pcl::PointCloud<PointType>::Ptr> global_map_frames;
        pcl::PointCloud<PointType>::Ptr global_map;
        std::deque<pcl::PointCloud<PointType>::Ptr> pcl_buffer;
        std::deque<double> frame_times;
        double first_frame_time=-1;

        pcl::KdTreeFLANN<PointType>::Ptr search_tree;

        std::deque<PointPoseInfo> ego_path;
        std::deque<PointPoseInfo> real_poses; //buffer for real_poses
        size_t cur_pose_idx=0; //current pose idx in the real_poses buffer
        PointPoseInfo *cur_pose;

        int empty_cnt=0;

        int map_cnt;
        double car_width;
        double car_heigth;
        bool if_to_debug;

    public:

        EgomotionRecorder(): nh("~"){
            //init memmory
            cur_pcl.reset(new pcl::PointCloud<PointType>());
            search_tree.reset(new pcl::KdTreeFLANN<PointType>());

            sub_full=nh.subscribe<sensor_msgs::PointCloud2>("/lidar_cloud_dynamic",800,&EgomotionRecorder::fullHandler,this);
            sub_poses=nh.subscribe<nav_msgs::Odometry>("/poses",200,&EgomotionRecorder::realPoseHandler,this);

            pub_frame=nh.advertise<sensor_msgs::PointCloud2>("/dynamic_surf",200);

            //load parameters:
            if(!getParameter("dynamic_detection/map_cnt",map_cnt)){
                ROS_WARN("Map_cnt not set, resetting to default: 100");
                map_cnt=100;
            }

            if(!getParameter("dynamic_detection/car_width",car_width)){
                ROS_WARN("Car_width not set, resetting to default: 11");
                car_width=11;
            }

            if(!getParameter("dynamic_detection/car_heigth",car_heigth)){
                ROS_WARN("Car_heigth not set, resetting to default: 6");
                car_heigth=6;
            }

            if(!getParameter("common/if_to_debug",if_to_debug)){
                ROS_WARN("If_to_debug not set, resetting to default: false");
                if_to_debug=false;
            }

        }


        void fullHandler(const sensor_msgs::PointCloud2ConstPtr &pcl_in){
            pcl::PointCloud<PointType>::Ptr new_full(new pcl::PointCloud<PointType>());
            pcl::fromROSMsg(*pcl_in,*new_full);
            pcl_buffer.push_back(new_full);    
            if(first_frame_time==-1){
                first_frame_time=pcl_in->header.stamp.toSec();
            }        
            frame_times.push_back(pcl_in->header.stamp.toSec());
        }

        void realPoseHandler(const nav_msgs::OdometryConstPtr &poseIn) {
            //callback registering incoming cutted point cloud
            PointPoseInfo new_pose;

            new_pose.qw=poseIn->pose.pose.orientation.w;
            new_pose.qx=poseIn->pose.pose.orientation.x;
            new_pose.qy=poseIn->pose.pose.orientation.y;
            new_pose.qz=poseIn->pose.pose.orientation.z;

            new_pose.x=poseIn->pose.pose.position.x;
            new_pose.y=poseIn->pose.pose.position.y;
            new_pose.z=poseIn->pose.pose.position.z;
            new_pose.time=poseIn->header.stamp.toSec();
            real_poses.push_back(new_pose);
            empty_cnt=0;
        }

        void processBox(){
            if(cur_pose_idx<=0){
                return;
            }
            PointPoseInfo *cur_pose=&real_poses[cur_pose_idx];
            PointPoseInfo *prev_pose=&real_poses[cur_pose_idx-1];
            Eigen::Vector3d cur_position(cur_pose->x,cur_pose->y,cur_pose->z);
            Eigen::Vector3d prev_position(prev_pose->x,prev_pose->y,prev_pose->z);
            Eigen::Vector3d trans=cur_position-prev_position;

            Eigen::Vector3d centroid = (cur_position + prev_position) / 2 + Eigen::Vector3d(0,0,car_heigth/2); //centroid coordinates
            PointType center;
            center.x=centroid.x();
            center.y=centroid.y();
            center.z=centroid.z();
            double trans_dist = trans.norm();
            double max_distance = sqrt((car_width * car_width /4) + (trans_dist * trans_dist / 4) + (car_heigth * car_heigth /4));

            std::vector<int> indices;
            std::vector<float> distances;
            search_tree->setInputCloud(global_map);
            search_tree->radiusSearch(center,max_distance,indices,distances,0);

            //create rectangle:
            Eigen::Vector2d ab(-trans.y(),trans.x());
            Eigen::Vector2d bc(trans.x(),trans.y());
            ab.normalize();
            ab*=car_width;
            Eigen::Vector2d A=Eigen::Vector2d(prev_position.x(),prev_position.y()) - ab/2;
            Eigen::Vector2d B=Eigen::Vector2d(prev_position.x(),prev_position.y()) + ab/2;
            Eigen::Vector2d C=B+bc;
            for(size_t ii=0;ii<indices.size();++ii){
                int idx=indices[ii];
                PointType *pt = &global_map->points[idx];
                if(pt->normal_z==-1){
                    //skip ground points
                    continue;
                }
                //ckeck if the point belongs to the box
                Eigen::Vector2d M(pt->x,pt->y);
                Eigen::Vector2d am = M-A;
                Eigen::Vector2d bm = M-B;
                if(abs(pt->z - centroid.z())<=car_heigth && ab.dot(am) >=0 && ab.dot(am) <= ab.dot(ab) && bc.dot(bm)>=0 && bc.dot(bm)<=bc.dot(bc)){
                    //point within the box:
                    int frame_id=int(pt->normal_x);
                    int pt_id=int(pt->normal_y);
                    global_map_frames[frame_id]->points[pt_id].normal_z=1;
                }
            }
            
        }

        void updateCurPose(){
            double cur_time=frame_times.front();
            cur_time-=first_frame_time;
            cur_pose_idx=0;
            while(cur_pose_idx<real_poses.size()-1){
                PointPoseInfo *current_pose=&real_poses[cur_pose_idx];
                PointPoseInfo *next_pose=&real_poses[cur_pose_idx+1];
                if(abs(cur_time-next_pose->time)<abs(cur_time-current_pose->time)){
                    ++cur_pose_idx;
                }
                else{
                    break;
                }
            }
            frame_times.pop_front();
        }

        void updatePose(){
            updateCurPose();
            cur_pose=&real_poses[cur_pose_idx];
        }

        pcl::PointCloud<PointType>::Ptr transformCloud(pcl::PointCloud<PointType>::Ptr &cloudIn, PointPoseInfo * PointInfoIn,long pcl_idx) {
            //corrects surface points and their normal vectors with last relative pose
            pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>());

            //extract last relative pose transformations
            Eigen::Quaterniond quaternion(PointInfoIn->qw,
                                        PointInfoIn->qx,
                                        PointInfoIn->qy,
                                        PointInfoIn->qz);
            Eigen::Vector3d transition(PointInfoIn->x,
                                    PointInfoIn->y,
                                    PointInfoIn->z);

            for (int i = 0; i < cloudIn->points.size(); ++i)
            {
                if(cloudIn->points[i].normal_z==-1){
                    //skip ground points
                    continue;
                }
                Eigen::Vector3d ptIn(cloudIn->points[i].x, cloudIn->points[i].y, cloudIn->points[i].z); //distorted point
                Eigen::Vector3d ptOut = quaternion * ptIn + transition; //corrected point

                //save corrected point
                PointType pt;
                pt.x = ptOut.x();
                pt.y = ptOut.y();
                pt.z = ptOut.z();
                pt.intensity = cloudIn->points[i].intensity;
                pt.curvature = cloudIn->points[i].curvature;
                //mark new point
                pt.normal_x = float(pcl_idx);
                pt.normal_y = float(i);
                cloudOut->push_back(pt);
            }
            return cloudOut;
        }

        void buildGlobalMap(){
            pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>());
            pcl::copyPointCloud(*cur_pcl,*cloud);
            global_map_frames.push_back(cloud);
            ego_path.push_back(*cur_pose);
            const pcl::PointCloud<PointType>::Ptr transformed_cloud=transformCloud(cur_pcl,cur_pose,global_map_frames.size()-1);
            global_map=transformed_cloud;
            long idx=global_map_frames.size()-2;
            long min_idx = global_map_frames.size() - 2 -map_cnt;
            long pose_idx = ego_path.size()-2;
            while (idx>=0 && idx>=min_idx && pose_idx>=0)
            {   
                PointPoseInfo *past_pose=&ego_path[pose_idx];
                pcl::PointCloud<PointType>::Ptr past_frame=global_map_frames[idx];
                pcl::PointCloud<PointType>::Ptr new_frame=transformCloud(past_frame,past_pose,idx);
                (*global_map)+=*new_frame;
                --idx;
                --pose_idx;
            }
        }

        void run(){
            if(pcl_buffer.empty() || real_poses.size()<=2){
                if(empty_cnt>= 1000 && global_map_frames.size()>=5){
                    reconstruct(0);
                }
                ++empty_cnt;
                return;
            }

            Timer t("Egomotion Recording");
            t.tic();
            cur_pcl=pcl_buffer.front();

            updatePose();
            buildGlobalMap();
            processBox();
            reconstruct(map_cnt);
            pcl_buffer.pop_front();
            if(if_to_debug){
                cout << "Process Egomotion Recording took: "<< t.toc() << endl;
            }
            
        }

        void reconstruct(size_t min_size){
            ros::Rate reconstruction_rate(10);
            double prev_time=-1;
            while(global_map_frames.size()>min_size){
                pcl::PointCloud<PointType>::Ptr oldest_frame=global_map_frames.front();
                if(real_poses.size()==0){
                    ROS_WARN("Empty real poses");
                    continue;
                }
                PointPoseInfo *oldest_pose=&real_poses.front();

                sensor_msgs::PointCloud2 msg_out;
                pcl::toROSMsg(*oldest_frame,msg_out);
                msg_out.header.frame_id="lili_om";
                if(global_map_frames.size()==1){
                    msg_out.header.frame_id="liliend";
                }
                msg_out.header.stamp=ros::Time(oldest_pose->time);
                pub_frame.publish(msg_out);

                if(if_to_debug){
                    ROS_INFO("SENT OUT: %lf with : %lu  points",oldest_pose->time,oldest_frame->points.size());
                }
                
                global_map_frames.pop_front();
                real_poses.pop_front();
                if(global_map_frames.size()>min_size){
                    reconstruction_rate.sleep();
                }
            }
        }
};

int main(int argc, char** argv) {
    ros::init(argc, argv, "lili_om");

    ROS_INFO("\033[1;32m---->\033[0m Egomotion Recording Started.");
    EgomotionRecorder recorder;
    ros::Rate rate(20);
    while(ros::ok()){
        ros::spinOnce();
        recorder.run();
        rate.sleep();
    }
    ros::spin();
    return 0;
}