#include <utils/common.h>
#include "livox_ros_driver/CustomMsg.h"

#define LIDAR1_CODE "/livox/lidar_1PQDH7600110791" 
#define LIDAR2_CODE "/livox/lidar_3WEDJ5B00100371"
#define LIDAR3_CODE "/livox/lidar_3WEDJ5B00100491"

#define PUBLISH_CODE "/livox_ros_points"

auto earlier=[](livox_ros_driver::CustomMsgConstPtr msg_left,livox_ros_driver::CustomMsgConstPtr msg_right){
    return msg_left->header.stamp< msg_right->header.stamp;
};

/* --------------------------------------------------------------------------------------

-convert incoming lidar clouds to sensor_msgs::PointCloud2 and fix translation distortion caused by lidar's position

ROS DRIVER POINT:

uint32 offset_time      # offset time relative to the base time
float32 x               # X axis, unit:m
float32 y               # Y axis, unit:m
float32 z               # Z axis, unit:m
uint8 reflectivity      # reflectivity, 0~255
uint8 tag               # livox tag
uint8 line              # laser number in lidar

----------------------------------------------------------------------------------------*/ 

ros::Publisher publisher;


class LidarCorrector
{
    private:
        Eigen::Vector3d translation;
        ros::Subscriber subscriber;
        ros::NodeHandle node_handle;
        ros::Publisher *pub_cloud;
        int id;
        string sub_code;
    public:

        void CorrectPoint(PointXYZINormal *pt_in){
            pt_in->x+=translation[0];
            pt_in->y+=translation[1];
            pt_in->z+=translation[2];
        }

        void LidarHandler(const livox_ros_driver::CustomMsgConstPtr& msg_in){
            pcl::PointCloud<PointXYZINormal> pcl_in;
            auto time_end = msg_in->points.back().offset_time;
            for (unsigned int i = 0; i < msg_in->point_num; ++i) {
                PointXYZINormal pt;
                pt.x = msg_in->points[i].x;
                pt.y = msg_in->points[i].y;
                pt.z = msg_in->points[i].z;
                float s = float(float(msg_in->points[i].offset_time) / (float)time_end);
                pt.intensity = msg_in->points[i].line + s*0.1; // integer part: line number; decimal part: relative time within its frame
                pt.curvature = 0.1 * msg_in->points[i].reflectivity;
                pt.normal_x=float(id);
                pt.normal_y=float(msg_in->points[i].line);
                pt.normal_z=0; //set to static by default -> will change in DynamicDetection
                CorrectPoint(&pt);
                pcl_in.push_back(pt);
            }

            ros::Time timestamp(msg_in->header.stamp.toSec());

            sensor_msgs::PointCloud2 pcl_ros_msg;
            pcl::toROSMsg(pcl_in, pcl_ros_msg);
            pcl_ros_msg.header.stamp = timestamp;
            pcl_ros_msg.header.frame_id = "lili-om";

            pub_cloud->publish(pcl_ros_msg);
        }

        
    
        LidarCorrector(ros::NodeHandle &nh,string sub_path, const double x, const double y, const double z, int id_,ros::Publisher *_pub){
            id=id_;
            translation=Eigen::Vector3d(x,y,z);
            sub_code=sub_path;
            subscriber=node_handle.subscribe<livox_ros_driver::CustomMsg>(sub_code,800,&LidarCorrector::LidarHandler,this);
            pub_cloud=_pub;
            ROS_INFO("Lidar corrector instance --- %d --- started",id);
        }
};

int main(int argc, char **argv){
    ros::init(argc,argv,"lili-om");
    ros::NodeHandle nh("~");
    publisher=nh.advertise<sensor_msgs::PointCloud2>(PUBLISH_CODE,700);

    LidarCorrector horizon_l(nh,LIDAR2_CODE,0,0,0,1,&publisher);
    LidarCorrector horizon_r(nh,LIDAR3_CODE,0,0,0,2,&publisher);
    LidarCorrector tele_15(nh,LIDAR1_CODE,0,0,0,3,&publisher);

    ROS_INFO("\033[1;32m---->\033[0m Lidar Correction Started.");
    ros::spin();
    return EXIT_SUCCESS;
}