#!/bin/bash

echo "Enter path to .npy point cloud:"
read filepath
echo "Enter fps count:"
read fps
if [ -f $filepath ] && [ $fps -gt 0 ] 
then 
	roslaunch lili_om playback.launch input_points:="$filepath" playback_rate:="$fps"
else
	echo "Invalid parameters entered"
fi 
 
