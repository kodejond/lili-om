#!/bin/bash


echo "Enter filepath of input numpy point cloud:"
read input_filepath
echo "Enter rviz playback rate:"
read playback_rate
if [ -f $input_filepath ] && [[ $input_filepath == *.npy ]]
then
	echo "Filepath is valid --> running deskewing"
	output_filepath=""
	output_filepath+="${input_filepath/%'.npy'/'-corrected.npy'}"
	echo "Deskewed point cloud will be saved to: $output_filepath"
	roslaunch lili_om deskew.launch input_points:="$input_filepath" playback_rate:="$playback_rate" output_points:="$output_filepath"
	echo "Deskewing finidhed"
else
	echo "Input file does not exist"
fi

