#!/bin/bash

#helper variables:

echo "Enter lvx filepath:"
read filepath
echo "Enter xml poses filepath:"
read poses_filepath
target_line="Read progress : 93"
if [ -f $filepath ] && [ -f $poses_filepath ] && [[ $filepath == *.lvx ]] && [[ $poses_filepath == *.xml ]]
then
	echo "File paths are valid --> starting conversion:"
	(
		#file conversion loop:
		roslaunch livox_ros_driver my_launch.launch lvx_file_path:="$filepath" > temp_logs.txt
		echo "CONVERSION HAS ENDED"
		exit
	)&
	(
		#conversion ending loop:
		sleep 30
		end="0"
		while [ $end == "0" ]
		do
			while read line
			do
				echo "$line"
				if [ "$target_line" == "$line" ]
				then
					echo "Ending line found --> Ending conversion"
					sleep 20
					pkill ros
					end+="1"
				fi
			done < 'temp_logs.txt'
			sleep 3
		done
		echo "CONVERSION TERMINATION HAS ENDED --> Starting egomotion recording"
		rm temp_logs.txt
		output_filepath=""
		rosbag_filepath=""
		lvx_str=".lvx"
		npy_str=".npy"
		bag_str=".bag"
		output_filepath+="${filepath/%$lvx_str/$npy_str}"
		rosbag_filepath+="${filepath/%$lvx_str/$bag_str}"
		echo "Egomotion recording to: $output_filepath"
		(roslaunch lili_om record_egomotion.launch input_poses:="$poses_filepath" output_points:="$output_filepath" kill_on_exit:="true"
                echo "Egmotion recording finished"
                )&
		(
			echo "Waiting fsor roslaunch"
			sleep 10
			echo "Playing bag file: $rosbag_filepath"
			rosbag play "$rosbag_filepath" -r 0.05

		)
	)
else
	echo "Entered file path is INVALID --> Cannot proceed with conversions"
fi







