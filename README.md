The code in this repository is code from https://github.com/KIT-ISAS/lili-om that was modified to perform scan-matching and de-skewing of dynamic objects

<h1>Usage</h1>
<ul>
    <li>Use bag_conversion.sh bash script to convert lvx file to rosbag, label points in ego-path as dynamic ( using .xml file containing vehicle poses ) and save to numpy point cloud </li>
    <li>Use deskew.sh bash script to de-skew dynamic objects and save corrected point clouds to numpy point cloud </li>
    <li>Use playback.sh bash script to play points from given numpy point clouds in rviz </li>

<h1>Dependency</h1>
<p>System dependencies ( tested on Ubuntu 20.04 ) </p>
<ul>
    <li>ROS ( tested on Noetic ) </li>
    <li>ceres ( Ceres solver 2.0 ) </li>
    <li>livox ros driver ( https://github.com/Livox-SDK/livox_ros_driver/releases/tag/v2.5.0 ) </li>
</ul>
<h1>Compilation</h1>
<p>Compilation using Catkin tools </p>
